function checkPassword(value) {
    var passwordError = "";
    const passwordPattern = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/
    if (value.test(passwordPattern)) {
        passwordError = ""
    } else {
        passwordError = "Mật khẩu phải chứa ít nhất 1 số, 1 chữ hoa, 1 chữ thường và độ dài ít nhất là 8 ký tự."
    }
    console.log(passwordError);
}
checkPassword("test123");