function timkiemvitrisv(listSv, maSv) {
    return listSv.findIndex(function (item) {
        return item.ma == maSv;
    });
}
var layThongtintuform = function () {
    var ma = document.getElementById("txtMaSV").value;
    var ten = document.getElementById("txtTenSV").value;
    var email = document.getElementById("txtEmail").value;
    var mk = document.getElementById("txtPass").value;
    var date = document.getElementById("txtNgaySinh").value;
    var kh = document.getElementById("khSV").value;
    var toan = document.getElementById("txtDiemToan").value * 1;
    var ly = document.getElementById("txtDiemLy").value * 1;
    var hoa = document.getElementById("txtDiemHoa").value * 1;
    var sv = new SinhVien(ma, ten, email, mk, date, kh, toan, ly, hoa);
    return sv;
};
var hienthithongtinlenform = function (sv) {
    document.getElementById("txtMaSV").value = sv.ma;
    document.getElementById("txtTenSV").value = sv.ten;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.mk;
    document.getElementById("txtNgaySinh").value = sv.date;
    document.getElementById("khSV").value = sv.kh;
    document.getElementById("txtDiemToan").value = sv.toan;
    document.getElementById("txtDiemLy").value = sv.ly;
    document.getElementById("txtDiemHoa").value = sv.hoa;
};
