var SinhVien = function (
    _ma,
    _ten,
    _email,
    _mk,
    _date,
    _kh,
    _toan,
    _ly,
    _hoa,

) {
    this.ma = _ma;
    this.ten = _ten;
    this.email = _email;
    this.mk = _mk;
    this.toan = _toan;
    this.ly = _ly;
    this.hoa = _hoa;
    this.date = _date;
    this.kh = _kh;
    this.tinhDtb = function () {
        return ((this.toan + this.ly + this.hoa) / 3).toFixed(2)
    };
};