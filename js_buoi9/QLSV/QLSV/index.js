function inKetQua() {
    var maSv = document.getElementById("txtMaSV").value;
    var tenSv = document.getElementById("txtTenSV").value;
    var loaiSv = document.getElementById("loaiSV").value;
    var toan = Number(document.getElementById("txtDiemToan").value);
    var van = Number(document.getElementById("txtDiemVan").value);
    var sv = {
        ma: maSv,
        ten: tenSv,
        loai: loaiSv,
        toan: toan,
        van: van,
        tinhDTB: function () {
            return (this.toan + this.van) / 2
        },
        xepLoai: function () {
            var dtb = this.tinhDTB()
            if (dtb >= 8 && dtb <= 10) {
                return "Gioi"
            }
            else if (dtb >= 5 && dtb <= 7) {
                return "kha"
            }
            else if (dtb >= 0 && dtb <= 4) {
                return "kem"
            }
            else {
                return 'sai du lieu'
            }
        }
    }
    var xLoai = sv.xepLoai()
    console.log(xLoai)
    var diemTb = sv.tinhDTB()
    console.log(diemTb)
    document.getElementById('spanDTB').innerHTML = diemTb
    document.getElementById('spanXepLoai').innerHTML = xLoai
    document.getElementById('spanTenSV').innerHTML = sv.ten
    document.getElementById('spanMaSV').innerHTML = sv.ma
    document.getElementById('spanLoaiSV').innerHTML = sv.loai
}
