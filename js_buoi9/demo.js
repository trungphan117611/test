var dog = {
    name: 'xuxu',
    age: 2,
    speak: function () {
        console.log("gau gau", this.name)
    }
}
dog.speak()
console.log(dog.age)
dog.age = 21
console.log(dog.age)

var dog2 = { ...dog }
console.log(dog2)
dog2.age = 20
console.log(dog2.age)
console.log(dog.age)
var height = "name"
var value = dog[height]
console.log('value', value)
