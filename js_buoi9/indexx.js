function inKetQua() {
    var maSv = document.getElementById('txtMaSV').value
    var tenSv = document.getElementById('txtTenSV').value
    var loaiSv = document.getElementById('loaiSV').value
    var toan = document.getElementById('txtDiemToan').value * 1
    var van = document.getElementById('txtDiemVan').value * 1
    sv = {
        ma: maSv,
        ten: tenSv,
        loai: loaiSv,
        toan: toan,
        van: van,
        tinhDtb: function () {
            return (this.toan + this.van) / 2
        },
        xepLoai: function () {
            var dtb = sv.tinhDtb()
            if (dtb >= 0 && dtb < 5) {
                return ("kem")
            }
            if (dtb >= 5 && dtb < 8) {
                return ("kha")
            }
            if (dtb >= 8 && dtb < 10) {
                return ("tot")
            }

        }

    }
    var dtb = sv.tinhDtb()
    var xl = sv.xepLoai()
    document.getElementById("spanDTB").innerHTML = dtb
    document.getElementById("spanXepLoai").innerHTML = xl


}