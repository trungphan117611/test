

function kiemtratrong(value, selectorerror, name) {
    if (value === '') {
        document.querySelector(selectorerror).innerHTML = name + 'kh duoc bo trong'
        return false
    } else {
        document.querySelector(selectorerror).innerHTML = ''
        return true
    }

}
function kiemtratatcakitu(value, selectorerror, name) {
    var regexletter = /^[A-Za-z]+$/;
    if (regexletter.test(value)) {
        document.querySelector(selectorerror).innerHTML = ""
        return true
    }
    document.querySelector(selectorerror).innerHTML = name + 'phai la chu cai'
    return false


}
function kiemtramail(value, selectorerror, name) {
    const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (
        re.test(value)
    ) {
        document.querySelector(selectorerror).innerHTML = ""
        return true
    }
    document.querySelector(selectorerror).innerHTML = name + 'phai la chu cai'
    return false
}
function kiemtrso(value, selectorerror, name) {
    var regexnumber = /^[0-9]+$/;
    if (
        regexnumber(value)
    ) {
        document.querySelector(selectorerror).innerHTML = ""
        return true
    }
    document.querySelector(selectorerror).innerHTML = name + 'phai la chu cai'
    return false

}