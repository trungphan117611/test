/*
MVC : 
Models: Chứa các file js định nghĩa kiểu dữ liệu (prototype hay class)
View: Chứa các giao diện html css ...
Controller: Chứa các file javascript dom trên giao diện đó

*/
var mangSinhVien = [];
document.getElementById("btnXacNhan").onclick = function () {
    var sinhVien = new SinhVien();
    sinhVien.maSinhVien = document.getElementById("maSinhVien").value;
    sinhVien.tenSinhVien = document.getElementById("tenSinhVien").value;
    sinhVien.loaiSinhVien = document.getElementById("loaiSinhVien").value;
    sinhVien.diemRenLuyen = document.getElementById("diemRenLuyen").value;
    sinhVien.email = document.getElementById("email").value;
    sinhVien.soDienThoai = document.getElementById("soDienThoai").value;
    sinhVien.diemToan = document.getElementById("diemToan").value * 1;
    sinhVien.diemLy = document.getElementById("diemLy").value * 1;
    sinhVien.diemHoa = document.getElementById("diemHoa").value * 1;

    var valid = true
    valid &= kiemtratrong(sinhVien.maSinhVien, '#error_required_maSinhVien', "ma sinh vien ")
    valid &= kiemtratrong(sinhVien.tenSinhVien, '#error_required_tenSinhVien', 'ten sinh vien')
    valid &= kiemtratatcakitu(sinhVien.tenSinhVien, '#error_required_tenSinhVien', 'ten sinh vien')





    mangSinhVien.push(sinhVien);

    luuStore(mangSinhVien, "mangSinhVien");
    var html = renderSinhVien(mangSinhVien);
    document.querySelector("tbody").innerHTML = html;
};
function renderSinhVien(arrSinhvien) {
    var html = "";
    for (var i = 0; i < arrSinhvien.length; i++) {
        var sv = arrSinhvien[i];
        html += `
            <tr>
                <td>${sv.maSinhVien}</td>
                <td>${sv.tenSinhVien}</td>
                <td>${sv.emailSinhVien}</td>
                <td>${sv.sodtSinhVien}</td>
                <td>${sv.loaiSinhVien}</td>
                <button class="btn btn-success">delete</button>
            </tr>`;
    }

    return html;
}

function luuStore(content, storeName) {
    var sContent = JSON.stringify(content);
    // bien doi obj hoac array thanh chuoi
    localStorage.setItem(storeName, sContent);
    //  dem chuoi do luu vao localStorage
}
function getStore(storeName) {
    var output;
    if (localStorage.getItem(storeName)) {
        output = JSON.parse(localStorage.getItem(storeName));
    }
    return output;
}
window.onload = function () {
    var content = getStore("mangSinhVien");

    if (content) {
        var html = renderSinhVien(content);
        console.log(html)
        document.querySelector("tbody").innerHTML = html;
    }
};
